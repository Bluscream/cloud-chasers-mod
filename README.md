# Cloud Chasers Mod Menu [Melonloader](https://melonwiki.xyz/#/?id=requirements) Mod for [Storm Chasers](https://steamdb.info/app/862800/info/) ![](https://img.shields.io/github/downloads/Bluscream/cloud-chasers-mod/total)

- If you want to fork and fix or update this, feel free to do so 🙏🏻 
- You can open the menu by pressing ESC ingame, or using the keybinds `ToggleModPanelKey` and `ToggleModPanelAltKey` from your `Storm Chasers\UsersData\MelonPreferences.cfg` file.


![](https://staticdelivery.nexusmods.com/mods/5076/images/5/5-1691873490-1757963709.png)
